#!/bin/bash
time makepkg -s --noconfirm
PKG=$(find /makemkv -name '*.pkg.tar.xz' -type f)
echo
echo "docker cp $(cat /etc/hostname):$PKG /home/felix/pkg/"

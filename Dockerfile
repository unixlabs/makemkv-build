FROM manjarolinux/base
RUN useradd build
RUN echo 'build:build' | chpasswd
RUN echo 'build ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
ADD makepkg.conf /etc/makepkg.conf
RUN pacman -Syyu --noconfirm
RUN pacman --noconfirm -S git vi vim base-devel
RUN git clone https://aur.archlinux.org/makemkv.git
COPY run.sh /makemkv/run.sh
RUN chown -R build.build /makemkv
USER build
WORKDIR /makemkv
ENTRYPOINT ["bash", "run.sh"]

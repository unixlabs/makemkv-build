all: build tag push
version := $(shell cat version)
build:
	docker build --no-cache -t makemkv-build .
tag:
	docker tag makemkv-build registry.gitlab.com/unixlabs/makemkv-build:${version}
	docker tag makemkv-build registry.gitlab.com/unixlabs/makemkv-build:latest
push:
	docker push registry.gitlab.com/unixlabs/makemkv-build:${version}
	docker push registry.gitlab.com/unixlabs/makemkv-build:latest
